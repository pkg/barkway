m4_define([barkway_major], [0])
m4_define([barkway_minor], [2020])
m4_define([barkway_micro], [1])
m4_define([barkway_nano], [0])

m4_define([barkway_base_version],
          [barkway_major.barkway_minor.barkway_micro])

m4_define([barkway_version],
          [m4_if(barkway_nano, 0,
                [barkway_base_version],
                [barkway_base_version].[barkway_nano])])

# Before making a release, the version info should be modified.
# Follow these instructions sequentially:
#   1. If the library source code has changed at all since the last update, then
#      increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#   2. If any interfaces have been added, removed, or changed since the last
#      update, increment current, and set revision to 0.
#   3. If any interfaces have been added since the last public release, then
#      increment age.
#   4. If any interfaces have been removed or changed since the last public
#      release, then set age to 0. Also increment barkway_major.
m4_define([barkway_lt_current], [0])
m4_define([barkway_lt_revision], [4])
m4_define([barkway_lt_age], [0])

m4_define([barkway_lt_version],
[barkway_lt_current:barkway_lt_revision:barkway_lt_age])

AC_PREREQ([2.62])
AC_INIT([barkway], barkway_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST(BARKWAY_VERSION,barkway_version)
AC_SUBST(BARKWAY_API_VERSION,barkway_major)

AM_INIT_AUTOMAKE([-Wno-portability tar-ustar])
AM_SILENT_RULES([yes])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT

AC_ARG_VAR(GDBUS_CODEGEN, [the gdbus-codegen programme ])
AC_PATH_PROG(GDBUS_CODEGEN, gdbus-codegen)
if test -z "$GDBUS_CODEGEN" ; then
  AC_MSG_ERROR([gdbus-codegen not found])
fi

#Check if Gobject-introspection enabled
GOBJECT_INTROSPECTION_CHECK([1.31.1])

dnl check for hotdoc
HOTDOC_CHECK([0.8], [c, dbus])

BARKWAY_PACKAGES_PUBLIC_GLIB="glib-2.0 gio-2.0"
BARKWAY_PACKAGES_PRIVATE_GLIB="gthread-2.0 gmodule-2.0 gio-unix-2.0"

BARKWAY_PACKAGES_PUBLIC_CANTERBURY=""
BARKWAY_PACKAGES_PRIVATE_CANTERBURY="canterbury-gdbus-0"

BARKWAY_PACKAGES_PUBLIC="$BARKWAY_PACKAGES_PUBLIC_GLIB $BARKWAY_PACKAGES_PUBLIC_CANTERBURY"
BARKWAY_PACKAGES_PRIVATE="$BARKWAY_PACKAGES_PRIVATE_GLIB $BARKWAY_PACKAGES_PRIVATE_CANTERBURY"

AC_SUBST([BARKWAY_PACKAGES_PUBLIC])
AC_SUBST([BARKWAY_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$BARKWAY_PACKAGES_PUBLIC_GLIB $BARKWAY_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([CANTERBURRY], [$BARKWAY_PACKAGES_PUBLIC_CANTERBURY $BARKWAY_PACKAGES_PRIVATE_CANTERBURY])
PKG_CHECK_MODULES([CANTERBURY_PLATFORM], [canterbury-platform-0 >= 0.1703.4])

GLIB_GSETTINGS

AX_COMPILER_FLAGS([], [],
                  dnl TODO: fix the warnings and change this from [yes] to
                  dnl [m4_if(barkway_nano, 0, [yes], [no])],
                  [yes])

BARKWAY_IFACE='-lbarkwayiface -lbarkwaylayeriface'
AC_SUBST(BARKWAY_IFACE)

AC_ARG_WITH([systemduserunitdir],
  [AC_HELP_STRING([--with-systemduserunitdir=DIR],
    [path to systemd service directory])],
  [systemduserunitdir="${withval}"],
  [systemduserunitdir='${prefix}/lib/systemd/user'])
AC_SUBST([systemduserunitdir])

# Installed tests
AC_ARG_ENABLE(modular_tests,
	 AS_HELP_STRING([--disable-modular-tests],
	 [Disable build of test programs (default: no)]),,
	 [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
	 AS_HELP_STRING([--enable-installed-tests],
	 [Install test programs (default: no)]),,
	 [enable_installed_tests=no])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
	 [test "$enable_modular_tests" = "yes" ||
	 test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")

# code coverage
AX_CODE_COVERAGE

AC_CONFIG_FILES([
Makefile
barkway.pc
docs/Makefile
docs/reference/Makefile
interface/Makefile
interface/barkway-iface/Makefile
interface/barkway-layer-iface/Makefile
src/Makefile
tests/Makefile
])

AC_OUTPUT


dnl Summary
echo "                      barkway "
echo "                      --------------
        documentation                  : ${enable_documentation}
        code coverage                  : ${enable_code_coverage}
        compiler warings               : ${enable_compile_warnings}
        Test suite                     : ${enable_modular_tests}
"
